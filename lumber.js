const lumber                      = Object.create(null);

lumber.lumber                     = 0;
lumber.amountOfFutureLumberValues = 10;
lumber.futureLumberValuesHtml     = '';

lumber.setLumber = function (newLumber) {
    lumber.lumber = newLumber;

    display.updateLumber();
    lumber.updateFutureLumberValuesHtml();
};

lumber.setAmountOfFutureLumberValues = function (amountOfFutureLumberValues) {
    lumber.amountOfFutureLumberValues = amountOfFutureLumberValues;
};

lumber.setFutureLumberValuesHtml = function (futureLumberValuesHtml) {
    lumber.futureLumberValuesHtml = futureLumberValuesHtml;
};

lumber.getLumber = function () {
    return lumber.lumber;
};

lumber.getAmountOfFutureLumberValues = function () {
    return lumber.amountOfFutureLumberValues;
};

lumber.getFutureLumberValuesHtml = function () {
    return lumber.futureLumberValuesHtml;
};

lumber.changeAmountOfFutureLumberValues = function (increment) {
    switch(increment) {
        case true:
            lumber.setAmountOfFutureLumberValues(lumber.getAmountOfFutureLumberValues() + 1);

            break;
        case false:
            lumber.setAmountOfFutureLumberValues(lumber.getAmountOfFutureLumberValues() - 1);

            break;
        default:
            console.log('lumber.changeFutureLumberValues(increment); argument has to be boolean');

            break;
    }

    lumber.updateFutureLumberValuesHtml();
};

lumber.updateLumber = function () {
    emulator.updateRuntime();
    lumber.addLumberByAmountOfWisps(wisp.getAmountOfFinishedWisps());
};

lumber.getLumberPerTick = function () {
    return (wisp.getAmountOfWisps() * 2) + (lumberjack.getAmountOfUpgrades() * wisp.getAmountOfWisps());
};

lumber.getLumberPerXTicks = function (tickAmount) {
    return lumber.getLumberPerTick() * tickAmount;
};

lumber.addLumberByAmountOfWisps = function (amountOfWisps) {
    lumber.setLumber(lumber.getLumber() + lumber.getLumberPerTickFromXWisps(amountOfWisps));
};

lumber.getLumberPerTickFromXWisps = function (amountOfWisps) {
    return (amountOfWisps * 2) + (lumberjack.getAmountOfUpgrades() * amountOfWisps);
};

lumber.updateFutureLumberValuesHtml = function () {
    lumber.setFutureLumberValuesHtml('');

    var X                          = 1;
    var amountOfFutureLumberValues = lumber.getAmountOfFutureLumberValues();
    var li                         = '';

    for(X; X <= amountOfFutureLumberValues; X++) {
        switch(X) {
            case 1:
                li = li + '<li>Your lumber in ' + X + ' minute is ' + (lumber.getLumber() + (lumber.getLumberPerXTicks(6) * X));

                break;
            default:
                li = li + '<li>Your lumber in ' + X + ' minutes is ' + (lumber.getLumber() + (lumber.getLumberPerXTicks(6) * X));

                break;
        }
    }

    lumber.setFutureLumberValuesHtml(li);
    display.updateFutureLumberValues();
};

lumber.addXLumber = function (lumberAmount) {
    lumber.setLumber(lumber.getLumber() + lumberAmount);
};

lumber.subtractXLumber = function (lumberAmount) {
    lumber.setLumber(lumber.getLumber() - lumberAmount);
};