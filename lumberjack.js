const lumberjack            = Object.create(null);

lumberjack.upgradeCost      = [60, 80, 100, 120, 140, 160, 180, 200];
lumberjack.amountOfUpgrades = 0;
lumberjack.investedLumber   = 0;

lumberjack.setAmountOfUpgrades = function (amountOfUpgrades) {
    lumberjack.amountOfUpgrades = amountOfUpgrades;
};

lumberjack.setInvestedLumber = function (investedLumber) {
  lumberjack.investedLumber = investedLumber;
};

lumberjack.getUpgradeCost = function () {
    return lumberjack.upgradeCost;
};

lumberjack.getAmountOfUpgrades = function () {
    return lumberjack.amountOfUpgrades;
};

lumberjack.getInvestedLumber = function () {
    return lumberjack.investedLumber;
};

lumberjack.changeAmountOfLumberjackUpgrades = function (increment) {
    switch(increment) {
        case true:
            lumberjack.payUpgradeCostIfPossible();
            lumberjack.setAmountOfUpgrades(lumberjack.getAmountOfUpgrades() + 1);

            break;
        case false:
            lumberjack.refundUpgradeCostIfPreviouslyPaid();
            lumberjack.setAmountOfUpgrades(lumberjack.getAmountOfUpgrades() - 1);

            break;
        default:
            console.log('lumberjack.changeAmountOfLumberjackUpgrades(increment); argument has to be boolean');

            break;
    }

    display.updateAmountOfLumberjackUpgrades();
    display.updateAmountOfLumberGainedPerTick();
    lumber.updateFutureLumberValuesHtml();
};

lumberjack.payUpgradeCostIfPossible = function () {
    var upgradeCost = lumberjack.getOutstandingUpgradeCost();

    if(lumber.getLumber() - upgradeCost >= 0) {
        lumber.subtractXLumber(upgradeCost);
        lumberjack.setInvestedLumber(lumberjack.getInvestedLumber() + upgradeCost);
    }
};

lumberjack.refundUpgradeCostIfPreviouslyPaid = function () {
    var upgradeCost    = lumberjack.getPreviousUpgradeCost();
    var investedLumber = lumberjack.getInvestedLumber();

    if(investedLumber >= upgradeCost) {
        lumber.addXLumber(upgradeCost);
        lumberjack.setInvestedLumber(investedLumber - upgradeCost);
    }
};

lumberjack.getOutstandingUpgradeCost = function () {
    if(lumberjack.getAmountOfUpgrades() <= 7) {
        return lumberjack.getUpgradeCost()[lumberjack.getAmountOfUpgrades()];
    }

    return 200;
};

lumberjack.getPreviousUpgradeCost = function () {
    if(lumberjack.getAmountOfUpgrades() <= 7) {
        return lumberjack.getUpgradeCost()[lumberjack.getAmountOfUpgrades() - 1];
    }

    return 200;
};