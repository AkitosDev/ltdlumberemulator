const wisp = Object.create(null);

wisp.stock = [10000];

wisp.getAmountOfWisps = function () {
    return wisp.stock.length;
};

wisp.getAmountOfFinishedWisps = function () {
    var index                 = 0;
    var amountOfWisps         = wisp.getAmountOfWisps();
    var amountOfFinishedWisps = 0;

    for(index; index < amountOfWisps; index++) {
        switch(wisp.stock[index]) {
            case 0:
                amountOfFinishedWisps++;

                wisp.stock[index] = 10000;

                break;
            default:
                wisp.stock[index] = wisp.stock[index] - 10;

                break;
        }
    }

    return amountOfFinishedWisps;
};

wisp.changeAmountOfWisps = function (increment) {
    switch(increment) {
        case true:
            wisp.stock.push(10000);

            break;
        case false:
            wisp.stock.pop();

            break;
        default:
            console.log('lumber.changeWisp(increment); argument has to be boolean');

            break;
    }

    display.updateAmountOfWisps();
    display.updateAmountOfLumberGainedPerTick();
    lumber.updateFutureLumberValuesHtml();
};

wisp.resetWispInternalIntervals = function () {
    var index         = 0;
    var amountOfWisps = wisp.getAmountOfWisps();

    for(index; index < amountOfWisps; index++) {
        wisp.stock[index] = 10000;
    }
};