const emulator          = Object.create(null);

emulator.runtime        = 0;
emulator.updateInterval = null;

emulator.setRuntime = function (runtime) {
    emulator.runtime = runtime;
};

emulator.setUpdateInterval = function (updateInterval) {
    emulator.updateInterval = updateInterval;
};

emulator.getRuntime = function () {
    return emulator.runtime;
};

emulator.getUpdateInterval = function () {
    return emulator.updateInterval;
};

emulator.updateRuntime = function () {
    emulator.setRuntime(emulator.getRuntime() + 0.01);
    display.updateRuntime();
};

emulator.startEmulator = function () {
    if(emulator.updateInterval === null) {
        emulator.setUpdateInterval(setInterval(lumber.updateLumber , 10));
    }
};

emulator.pauseEmulator = function () {
    clearInterval(emulator.updateInterval);

    emulator.setUpdateInterval(null);
};

emulator.resetEmulator = function () {
    emulator.pauseEmulator();
    lumber.setLumber(0);
    emulator.setRuntime(0);
    wisp.resetWispInternalIntervals();
    display.updateLumber();
    display.updateRuntime();
    lumber.updateFutureLumberValuesHtml();
    emulator.startEmulator();
};