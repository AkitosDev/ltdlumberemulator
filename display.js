const display = Object.create(null);

display.updateAmountOfWisps = function () {
    document.getElementById('amountOfWisps').innerHTML = 'Wisps: ' + wisp.getAmountOfWisps();
};

display.updateAmountOfLumberjackUpgrades = function () {
    document.getElementById('amountOfLumberjackUpgrades').innerHTML = 'Lumberjack upgrades: ' + lumberjack.getAmountOfUpgrades();
};

display.updateAmountOfLumberGainedPerTick = function () {
    document.getElementById('amountOfLumberGainedPerTick').innerHTML = 'Lumber gained per tick: ' + lumber.getLumberPerTick();
};

display.updateLumber = function () {
    document.getElementById('lumber').innerHTML = 'Your current lumber is ' + lumber.getLumber();
};

display.updateRuntime = function () {
    document.getElementById('runtime').innerHTML = '<li>Running for ' + Math.floor(emulator.getRuntime()) + ' seconds</li>';
};

display.updateFutureLumberValues = function () {
    document.getElementById('futureLumberValues').innerHTML = lumber.getFutureLumberValuesHtml();
};

display.clearAddLumberInput = function () {
    document.getElementById('addLumber').value = '';
};

display.clearSubtractLumberInput = function () {
    document.getElementById('subtractLumber').value = '';
};

display.clearChangeLumberInput = function () {
    document.getElementById('changeLumber').value = '';
};