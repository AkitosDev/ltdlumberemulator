const eventHandler = Object.create(null);

eventHandler.setEventListeners = function () {
    document.getElementById('start')         .addEventListener('click', eventHandler.start);
    document.getElementById('pause')         .addEventListener('click', eventHandler.pause);
    document.getElementById('reset')         .addEventListener('click', eventHandler.reset);
    document.getElementById('addWisp')       .addEventListener('click', eventHandler.addWisp);
    document.getElementById('removeWisp')    .addEventListener('click', eventHandler.removeWisp);
    document.getElementById('addLJ')         .addEventListener('click', eventHandler.addLJ);
    document.getElementById('removeLJ')      .addEventListener('click', eventHandler.removeLJ);
    document.getElementById('showMore')      .addEventListener('click', eventHandler.showMore);
    document.getElementById('showLess')      .addEventListener('click', eventHandler.showLess);

    document.getElementById('addLumber')     .addEventListener('keypress', eventHandler.addLumber);
    document.getElementById('subtractLumber').addEventListener('keypress', eventHandler.subtractLumber);
    document.getElementById('changeLumber')  .addEventListener('keypress', eventHandler.changeLumber);
};

eventHandler.start = function () {
    emulator.startEmulator();
};

eventHandler.pause = function () {
    emulator.pauseEmulator();
};

eventHandler.reset = function () {
    emulator.resetEmulator();
};

eventHandler.addWisp = function () {
    if(wisp.getAmountOfWisps() < 7) {
        wisp.changeAmountOfWisps(true);
    }
};

eventHandler.removeWisp = function () {
    if(wisp.getAmountOfWisps() > 1) {
        wisp.changeAmountOfWisps(false);
    }
};

eventHandler.addLJ = function () {
    if(lumberjack.getAmountOfUpgrades() < 15) {
        lumberjack.changeAmountOfLumberjackUpgrades(true);
    }
};

eventHandler.removeLJ = function () {
    if(lumberjack.getAmountOfUpgrades() > 0) {
        lumberjack.changeAmountOfLumberjackUpgrades(false);
    }
};

eventHandler.showMore = function () {
    if(lumber.getAmountOfFutureLumberValues() < 20) {
        lumber.changeAmountOfFutureLumberValues(true);
    }
};

eventHandler.showLess = function () {
    if(lumber.getAmountOfFutureLumberValues() > 0) {
        lumber.changeAmountOfFutureLumberValues(false);
    }
};

eventHandler.addLumber = function (event) {
    if(event.keyCode === 13) {
        var lumberAmount = Number(document.getElementById('addLumber').value);

        if(isNaN(lumberAmount) === false) {
            lumber.addXLumber(lumberAmount);
            display.clearAddLumberInput();
        }
    }
};

eventHandler.subtractLumber = function (event) {
    if(event.keyCode === 13) {
        var lumberAmount = Number(document.getElementById('subtractLumber').value);

        if(isNaN(lumberAmount) === false && lumber.getLumber() - lumberAmount >= 0) {
            lumber.subtractXLumber(lumberAmount);
            display.clearSubtractLumberInput();
        }
    }
};

eventHandler.changeLumber = function (event) {
    if(event.keyCode === 13) {
        var newLumber = Number(document.getElementById('changeLumber').value);

        if(isNaN(newLumber) === false && newLumber >= 0) {
            lumber.setLumber(newLumber);
            display.clearChangeLumberInput();
        }
    }
};